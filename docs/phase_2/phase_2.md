## Phase 2: projects

## Suggestions:

### Eudyptula Challenge  

<http://eudyptula-challenge.org/>  

### OS Islene's Labs (not meant for upstream)  

<http://www.ic.unicamp.br/~islene/1s2017-mc504/proj2/proj2.html>  

### EVoC  

<https://www.x.org/wiki/XorgEVoC/> (LEVEL: Variable)

### DRM TODO list  

* <https://www.kernel.org/doc/html/latest/gpu/todo.html> (LEVEL: Variable)  

* Convert AST to Atomic modesetting (hardware? Palmeto at lsc?) (LEVEL:  Medium.)  

### Bugs  

Ask the organizers, we might have some bugs for you ;)  

* Investigate, replace and try to drop hwmon_device_register.  I.e.: (LEVEL:  Beginner.)  

(NULL device \*): hwmon_device_register() is deprecated. Please convert the driver to use *hwmon_device_register_with_info()*.  

* Drop usb/chipidea dependency on OF:  chipidea is usable on x86 platforms (razr-i-work) but it has a hardcoded dependency on OF which is not needed and causes those platforms to fail probe.  There is a scratch patch available, but it is not suitable for submission.  

<https://git.krisman.be/?p=linux.git;a=commit;h=c59e09e43d4473664030cc1cdcd55f3fc5130c44>  (LEVEL: Beginner)

* Bugs.freedesktop.org  (LEVEL: Variable)  

### Serial layer/8250

* Handle setserial ioport misconfiguration gracefully on !x86* architectures (LEVEL: Medium)  

### V4L2: VIMC  

* HDMI input submodule (LEVEL: medium)

* S-VIDEO input submodule (LEVEL: medium)

* Add frame rate control (LEVEL: beginner/medium) - **WIP** - Guilherme Gallo

* Optimize pipeline by generating image on capture device (LEVEL: medium) - **WIP** - Lucas Magalhães

* Add cropping with the Selection API  (LEVEL: medium)

### V4L2: VIVID  

* Proper implementation of connect and disconnect of HDMI input: events are not emulated by vivid  (LEVEL: medium)

* Loopback driver: allow interconnection between vivid instances, useful for more complex hardware (several HDMI inputs/outputs)  (LEVEL: medium)

* Emulation for metadata (histogram)   (LEVEL: medium)

* Software defined radio output  (LEVEL: medium)

* Improve timestamp system  - **WIP** - Gabriel Mandaji

### V4L2: Documentation  

* Per extended controls in a separate documentation  (LEVEL: beginner)
* Re-organizing and improving the selection API docs (LEVEL: beginner)

### V4L2: M2M (memory-to-memory) framework

* Add support for multiple planes in M2M framework (LEVEL: medium)

#### CEC compliance tests: raspberry-pi + CEC TV (userspace)

* In-depth tests of CEC features, step-by-step: timing, tuning, etc (multiple people for each feature) (LEVEL: medium)

### V4L2: convert soc_camera (hardware required)

* Soc_camera needs to be removed, the implementation of the sensors needs to be converted to use the framework properly. Check how the mt9t112 was done. (LEVEL: medium/hard)  

### V4L2: improve tests (userspace)

* v4l2-ctl: helpers for testing streaming, similar to v4l2-compliance

* Improve v4l2-ctl and v4l2-compliance for m2m devices

### V4L2: Others

<https://www.linuxtv.org/wiki/index.php/Media_Open_Source_Projects:_Looking_for_Volunteers>  

### Run the mainline kernel on Android phones with a GNU userspace.

Requires a lot of hacking all around the kernel.

Available Hardware:

* Razr-I: boots ok, DRM bugs.
* N900: Almost fully supported.
* Moto E Condor
* Moto X Ghost
* HTC Desire HD
* Your own phone!   

LEVEL: Medium, Hard.

### Sw_sync: fences

* Add support for programing the behavior of fences in the sw_sync driver, a way to inject failure to allow testing when fences fails. (LEVEL: medium)

### Kernel-ci: add smoke tests (userspace)

* Add a kernel-ci test for each kernel subsystem (LEVEL: variable)

### linux-next

Run linux-next and report problems found there.
It is easy to find kernel build warnings to fix or other issues.

<https://www.kernel.org/doc/man-pages/linux-next.html>

### vrealloc()

* vrealloc() -> realloc memory without copying, but
just extenting the page.
See if we have use for it, if it make sense

### Network File System

* NFS -> writting mode, then make  the network go down,
it starts closing NFS and call fsync but fsync is
unkillable (so it doesn't respond to a fatal signal)
and it hangs forever.
fsync sends the command over the network and wait for
the response that never arrives. So this wait needs to
become a killable wait.
TODO: see if this is still the case, this can be emulated using a VM.
https://github.com/kdave/xfstests/tree/master/test://github.com/kdave/xfstests/tree/master/tests

### aio

* work/test/verify this https://patchwork.kernel.org/patch/10110007/


### GCC flag - WIP - Leonardo Brás
* -Wshadow=local
turn this flag on in the build and fix the warnings.

```C
	int f(int a, int b) {
		if (a) {
			int b = 3; // this is shadowing the previous defined variable b
			g(a, b);
		}
	}
```

### remove IDR

xarray -> project to replace the radix tree https://lwn.net/Articles/745073/

This is more advanced and it is blocked by the xarray,
but will require analysis of individual drivers

### Debug wlist - WIP - Leonardo Brás

 http://git.infradead.org/users/willy/linux-dax.git/shortlog/refs/heads/wlist

test suite passes, but the kernel doesn't boot, it hangs at some point.

### vm_fault_t

converting fault handlers from returning plain INTs into a new vm_fault_t
The next set of things to do in this area is to convert the remaining callers of vm_insert_page to call vmf_insert_page. This is going to be tricky because they or their callers are expecting errnos today. So it's going to be painstaking work, not something to be done in a hurry.

### More ideas

<https://www.labbott.name/blog/2016/08/15/ideas-for-getting-started-in-the-linux-kernel/>

Check TODO files:

    find -name "*TODO*"
